<?php

/**
 * Display the contents of an array in a formatted way for debugging purposes.
 *
 * @param mixed $array  The array or variable to debug.
 *
 * @return void
 */
function debug($array): void
{
    echo '<pre style="height:200px;overflow-y: scroll;font-size: .5rem;padding: .6rem;font-family: Consolas, Monospace; background-color: #000;color:#fff;">';
    print_r($array);
    echo '</pre>';
}


/**
 * Redirect to a 404 Not Found page.
 *
 * @param bool $admin  Whether it's an admin page.
 *
 * @return void
 */
function redirectNotFound(bool $admin = false): void
{
    header("HTTP/1.0 404 Not Found");

    $location = ($admin) ? '../404.php' : '404.php';
    header("Location: $location");

    exit(); // Terminates the script after the redirection.
}


/**
 * Check if the current page matches the provided page name and, optionally, if it's in the admin directory.
 *
 * @param string $page            The page name to check.
 * @param bool   $isAdmin         Whether to check if it's in the admin directory.
 * @param string $directoryAdmin  The admin directory name.
 *
 * @return bool
 */
function isWebpack(string $page, bool $isAdmin = false, string $directoryAdmin = 'admin'): bool
{
    $currentRoute = $_SERVER['PHP_SELF'];
    $currentFileName = basename($currentRoute, '.php');

    if ($currentFileName === $page) {
        if ($isAdmin && str_contains($currentRoute, $directoryAdmin)) {
            return true;
        } elseif (!$isAdmin) {
            return true;
        }
    }

    return false;
}


/**
 * Display JSON response and terminate the script.
 *
 * @param mixed $data  The data to be encoded as JSON.
 *
 * @return void
 */
function showJson($data): void
{
    header('Content-type: application/json');
    $json = json_encode($data);

    if ($json !== false) {
        die($json);
    } else {
        die('Error in JSON encoding');
    }
}


/**
 * Check if the request is an AJAX request. Optionally, redirect to a 404 page if not AJAX and isAdmin is true.
 *
 * @param bool $isAdmin  Whether it's an admin page.
 *
 * @return bool
 */
function isAjaxRequest(bool $isAdmin = false): bool
{
    if (strtolower(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest') {
        return true;
    }

    if ($isAdmin) {
        redirectNotFound(true);
    }

    return false;
}
