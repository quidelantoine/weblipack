<?php

// Démarrage de la session
session_start();

// Inclusion des fichiers nécessaires
// require_once 'vendor/autoload.php';
// require_once 'core/pdo.php';
require_once 'parameters.php';
require_once 'core/func-base.php';
require_once 'core/request.php';
require_once 'core/validation.php';

